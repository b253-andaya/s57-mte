let collection = [];

// Write the queue functions below.

function dequeue() {
  if (collection.length === 0) {
    return null;
  }
  const firstElement = collection[0];
  collection = collection.slice(1);
  return firstElement;
}

module.exports = {
  enqueue: function (item) {
    collection.push(item);
    return collection;
  },
  dequeue: dequeue,
  front: function () {
    return collection[0];
  },
  size: function () {
    return collection.length;
  },
  isEmpty: function () {
    return collection.length === 0;
  },
  print: function () {
    console.log(collection);
    return collection;
  },
};
